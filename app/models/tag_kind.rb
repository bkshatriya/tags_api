class TagKind < ApplicationRecord
  has_many :tags, dependent: :destroy

  validates_uniqueness_of :name
end
