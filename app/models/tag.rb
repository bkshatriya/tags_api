class Tag < ApplicationRecord
  validates_uniqueness_of :name, scope: :tag_kind_id
end
