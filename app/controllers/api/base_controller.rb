class Api::BaseController < ApplicationController
  acts_as_token_authentication_handler_for User, fallback: :none, unless: lambda { |controller| 

    (controller.controller_name == "sessions" && controller.action_name == "create") || 
    (controller.controller_name == "registrations")
  }

  def autheticate_api_user!
    render json: { error: "Invalid token!!!" }, status: 401 unless current_user.present?
  end
end