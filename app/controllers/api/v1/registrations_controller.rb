class Api::V1::RegistrationsController < Api::BaseController
 
  def create
    user = User.new user_params
    
    if user.save
      response.headers["X-User-Token"] = user.authentication_token
      response.headers["X-User-Email"] = user.email

      render json: { success: true }
    else
      render json: { errors: user.errors.full_messages }, status: 422
    end
  end

  protected
    def user_params
      params.require(:user).permit(:email, :password)
    end
end