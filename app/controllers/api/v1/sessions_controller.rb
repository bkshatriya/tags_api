class Api::V1::SessionsController < Devise::SessionsController
  # login API
  def create
     self.resource = warden.authenticate!(auth_options)
     sign_in(resource_name, resource)
     respond_with_authentication_token(resource)
  end

  protected

    def respond_with_authentication_token(resource)
      response.headers["X-User-Token"] = resource.authentication_token
      response.headers["X-User-Email"] = resource.email

      render json: {
        success: true,
      }
    end
end