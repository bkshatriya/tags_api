Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, only: []
  devise_scope :user do
    post "/api/v1/users/sign_in" => "api/v1/sessions#create"
    post "/api/v1/users/sign_up" => "api/v1/registrations#create"
  end


  namespace :api do
    namespace :v1 do
      resources :tag_kinds, except: [:new, :edit] do
      end
      
      resources :tags, except: [:new, :edit]
    end
  end
end
