class CreateTag < ActiveRecord::Migration[5.1]
  def change
    create_table :tag_kinds do |t|
      t.string :name
    end

    create_table :tags do |t|
      t.string :name
      t.integer :tag_kind_id
    end
  end
end
